# Démonstrateur déploiement de services

## Pré-requis

Pour pouvoir mettre en oeuvre le démonstrateur, il faut les pré-requis suivants:

- avoir un compte sur le GitLab et des droits sous un Group représentant un service
- avoir installé git bash, pour récupérer le projet, le commiter, le tagger et le pusher
- avoir un éditeur de type VSCODE, pour éditer les sources et simplifier l'usage de git

## Création de son environnement sur le poste de travail

Se placer dans un group ou sous-groupe, puis créer un nouveau projet \<mon-projet> sous Gitlab à partir de l'import *d'un repository by URL*, <https://gitlab.com/DNUM-DETN-GSIL/demonstrateur-ci-cd.git>,

![Import project from URL](/html/asset/images/importProject.png)

puis le récupérer sur son poste de travail avec la commande git clone

    git clone https://gitlab-forge.din.developpement-durable.gouv.fr/<mon-group>/<mon-projet>.git

Editer le fichier config sous le répertoire .git, afin d'intégrer votre tocken d'accès à l'url de mise à jour de votre projet.

![Create personal access token](/html/asset/images/personalAccessToken.png)

## Contenu du démonstrateur

    +-+-> docker
    | |-+-> nginx
    | | |---> Dockerfile                     # fichier YAML docker de création de l'image NGINX à pousser dans la registry GitLab
    | |---> docker-compose-demonstrateur.yml # fichier YAML de déploiement des service sur le CAAS Eco4 de GSIL
    | |---> testid                           # clé cryptée de connection au bastion ssh du CaaS de GSIL
    | -+-> html
    |  |---> index.html                      # source à modifier pour voir les changements se déployer sur le CaaS
    |---> .gitignore                         # fichier pour éviter de commiter sur GitLab des fichiers inutiles
    |---> .gitlab-ci.yml                     # fichier YAML décrivant les jobs à effectuer pour l'intégration et le déploiement continus
    |---> README.md                          # ce fichier d'explication

## Paramétrage du projet sous GitLab

Il faut soit créer une nouvelle branche non protégée ou passer en unprotected la branche principale (main).
Dans les paramétres de votre projet, plusieurs actions sont nécessaires:

- permettre les push et merge de votre branche (Settings>Repository)

![unprotect branche](/html/asset/images/unprotectBranche.png)

- activer les "Shared runners" (Settings>CI/CD) et vérifier qu'au moins un runner actif dessert le tag "selfservice"

![active shared runner](/html/asset/images/sharedRunner.png)

- créer les variables de projets suivantes:

  - INPUTMASK fghijklmnopqrstuvwxyzabcdeMNOPQRSTUVWXYZABCDEFGHIJKL4567890123
  - IP_BASTION 10.165.77.17
  - OUTPUTMAK abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789
  - USER_BASTION debian
  - service_name nom-du-domaine

![variables du projet CI/CD](/html/asset/images/variablesCI-CD.png)

## Premier test

Il suffit de tagger le commit actuel avec un TAG commençant par P_, puis de réaliser un push vers GitLab pour entrainer le premier CI/CD. Sur git lancer les commandes suivantes:

  git add .
  git commit -m "version initiale"
  git tag -m "Version initiale" P_1.0.0 main
  git push main P_1.0.0

Sur GitLab, 2 jobs vont s'enchainer, en intégration continue, la génération de l'image que vous retrouverer sur GitLab, "Packages and registries>Container registry",

![Container registry GitLab](/html/asset/images/containerRegistry.png)

 puis le déploiement, qui consiste à:

- réserver les ports exposés par les services de la stack à déployer
- remplacer les mots clés {key} passer en paramétre avec l'option -s dans .gitlab-ci.yml
- déployer la stack de service du fichier docker-compose.yml complété
- créer un loadblancer pour les ports 80/443 exposés, avec un membre par node SWARM manager (minimum 3)
- créer un recordset sur le DNS ayant pour nom de domaine, soit preprod-<service_name>.gsil.eco4.cloud.e2.rie.gouv.fr associé à l'IP du load-balancer

En clair, à la fin de pipeline CI/CD, vous pouvez accèder à votre test en préprod, sur le lien http://preprod-<service_name>.gsil.eco4.cloud.e2.rie.gouv.fr/.

## Vérification sur GitLab

Sur GitLab, vous pouvez consulter le bon déroulement du pipeline. En cas d'erreur, vous recevez un mail, dont le sujet est du type "Nom-projet | Failed pipeline for P_1.0.1 | a6295c9a". En cliquant sur les jobs, vous avez la trace du déroulement du job avec les erreurs qui ont pu se produire.

![erreur GitLab](/html/asset/images/erreurGitLab.png)

![detail erreur job](/html/asset/images/detailErreur.png)

Si tout ce passe bien, sur "Packages and registries/Container registry", vous allez trouver les images générées lors du job build d'intégration. C'est cette image qui effectue le service sur un des nodes SWARM.

![Package Registry GitLab](/html/asset/images/packageRegistry.png)

## Mise en production

Il suffit de créer le tag de la version sans le P_, puis de publier la version taggée. Seul le déploiement est effectué, avec l'image construite lors du pipeline sur la version P_. La preprod est donc un passage obligatoire, afin de s'assurer que vos stack de service à container se déploit correctement. La preprod permettra de faire simplement des tests avec des images de bases plus récentes avant de les basculer sur la production. En clair, si tout ce passe correctement sur la préprod, faire simplement:

  git tag -m "Version production" 1.0.0 main
  git push main 1.0.0

A la fin de pipeline CI/CD, vous pouvez accèder à votre test en prod, sur le lien http://<service_name>.gsil.eco4.cloud.e2.rie.gouv.fr/.
